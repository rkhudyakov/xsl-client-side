/**
 * Post object for render articles inside of
 *
 * example of usage:
 *
 * new Post(ParentObject, postTplData, 'posts' , 2);
 *
 *
 * author: Roman Khudyakov | 2013
 */


var Post = (function(window){

/** object with default params */
	var DEFAULTS = {
		dataExtension : '.xml',
		backLinkCssClass : 'post__backLink'
	};

	/**
	 * Post constructor
	 * @param owner {Object} a parent object of which Post depends on
	 * @param tpl {DocumentFragment} a DocumentFragment with xsl template
	 * @param dataName {String} a name for ajax resource, like 'post' where it will be loading data from '/posts/{id}.xml'
	 * @param id {Number} an id of the item in the database
	 * @constructor
	 */
	function Post(owner, tpl, dataName, id){
		this.owner 		= owner;
		this.tpl 		= tpl;
		this.dataName 	= dataName;
		this.id 		= id;
		this.options 	= DEFAULTS;
		this.init();
	}

	Post.prototype.init = function(){
		this.renderPost(this.id);
	};

	/**
	 * Render post by id
	 * @param id {Number} and Id for server resource '/posts/{id}.xml'
	 */
	Post.prototype.renderPost = function(id){
		var result,
			self = this;

		// get data for post
		window.helper.ajax('GET', self.dataName + '/' + id + self.options.dataExtension , {}, function(data){

			// render the post
			result = window.helper.xsltJobber(self.tpl, data);

			// save the DOM container
			self.owner.activePostCnt = self.cnt = result;

			// append a result to dom
			self.owner.cnt.appendChild(result);

			// attach events to elements
			attachEvents.call(self);

			window.helper.formatTime(self.cnt.getElementsByTagName('time'));

			self.owner.changeState(2);
		});

	};

	/**
	 * Attach events for buttons or other interactive stuff
	 */
	function attachEvents(){
		var backLink,
			self = this;

	 	backLink = self.cnt.getElementsByClassName(self.options.backLinkCssClass)[0];
		backLink.addEventListener('click', handler, false);

		function handler(e){
			e.preventDefault();

			self.owner.changeState(1);
			return false;
		}
	}

	return Post;
})(window);