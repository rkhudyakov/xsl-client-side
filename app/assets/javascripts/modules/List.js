/**
 * A List object for displaying an list-like items
 * gets data from xml and applying xslt template for HTML rendering
 *
 * example of usage:
 *
 *
 * <div class="list-wrapper js-news-list loading">
 *		<i class="loading__indicator">Loading&hellip;</i>
 * </div>
 *
 * 	var NewsList = new List(document.getElementsByClassName('js-news-list'), 'posts');
 *
 *  it will be loading xml form /posts.xml
 *  and template as /list.xsl by default
 *
 *  author: Roman Khudyakov | 2013
 */


var List = (function(window){

/**	An object with default options */
	var DEFAULTS = {

/** Name for list template  */
		tpl: 'list',

/** Name for single post template */
		postTpl: 'post',

		tplExtension: '.xsl',
		dataExtension : '.xml',

/** a class name to add in the loading state */
		loadingCssClass: 'loading',

/** a css class for links */
		linkCssClass: 'list__item__linkArea',

/** empty state text */
		emptyText: '<li>There are no news for today</li>'
	};

	/**
	 * List constructor
	 * @param element {Element} a element/elements to attach a list to
	 * @param dataName {String} a path name for a ajax for get a data
	 * @param options {Object} a object with options for override DEFAULTS
	 * @constructor
	 */
	function List(element, dataName, options){

		this.instances = []; // save instances for later use just in case


 		if(typeof(element.length) !== 'undefined'){
		 	for(var i = 0, len = element.length; i < len; i++){
				this.instances.push(new List(element[i], dataName, options));
			}
		} else {
			this.cnt = element;
			this.dataName = dataName;

			// extend incoming options with defaults
			this.options =  options ? window.helper.extend(options, DEFAULTS) : DEFAULTS;
			this.init();
			this.instances.push(this);
		}
	}

	/**
	 * initialize the List by getting data and template
	 * and populating items afterwards
	 */
	List.prototype.init = function(){
		var self = this;

		// set state with spinner
		self.changeState(0);

		// getting data for our components


		// get xml data
		rest(self.dataName + self.options.dataExtension, function(data){
			self.data = data;

			// get xsl template
			rest(self.options.tpl + self.options.tplExtension, function(data){
				self.tplData = data;
				self.populateItems();
				attachEventsToLinks.call(self, self.links);
				self.changeState(1);
			});
		});

	};

	/**
	 * Render items with XSLTProcessor and attach events
	 * @param data {Document} Optional, XML document to render
	 */
	List.prototype.populateItems = function(data){
		var result;
		data = this.data || data;

		result = window.helper.xsltJobber(this.tplData, data);

		// save the parent element
		this.list = result;
		this.cnt.appendChild(result);

		this.links = this.cnt.getElementsByClassName(this.options.linkCssClass);

		if(this.links.length === 0){
			this.list.innerHTML = this.options.emptyText;
		} else {
			window.helper.formatTime(this.cnt.getElementsByTagName('time'));
		}
	};

	/**
	 * change state
	 *  0: 'Loading state' with spinner indicator
	 *  1: 'Normal state'
	 *  2: 'show Post, hide list'
	 * @param state
	 */
	List.prototype.changeState = function(state){
		switch(state){
			case 0:  // loading state with spinner
				window.helper.addClass.call(this.cnt, this.options.loadingCssClass);
				if(typeof(this.list) !== 'undefined'){
					this.list.style.display = 'none';
				}
				break;

			case 1: // normal list state, hide spinner, show list, remove post
				window.helper.removeClass.call(this.cnt, this.options.loadingCssClass);

				// display list if hidden
				if(typeof(this.list) !== 'undefined'){
					this.list.style.display = 'block';
				}

				// remove post if already rendered
				if(typeof(this.activePostCnt) !== 'undefined'){
					this.cnt.removeChild(this.activePostCnt);
				}
				break;

			case 2: // hide list, usually display post as well
				window.helper.removeClass.call(this.cnt, this.options.loadingCssClass);
				this.list.style.display = 'none';
				break;
		}
	};

	/**
	 * Show post by given ID in server resources
	 * @param id {Number}
	 */
	List.prototype.showPost = function(id){
		var self = this;

		// get the post template first time and save it
		if(typeof(self.postTplData) === 'undefined'){

			rest(self.options.postTpl + self.options.tplExtension, function(postTplData){
				self.postTplData = postTplData;
				initPost.call(self, postTplData, id);
			});

		} else {

			//otherwise calling new post with saved post template
			initPost.call(self, self.postTplData, id);
		}

	};


	/**
	 * init new post object
	 * @param postTplData {DocumentFragment} a post template
	 * @param id {Number} an ID for server resource
	 */
	function initPost(postTplData, id) {
		new Post(this, postTplData, this.dataName, id);
	}

	function attachEventsToLinks(links){
		var i = 0,
			len = links.length,
			self = this;

		for(; i < len; i ++){
			links[i].addEventListener('click', handler, false);
		}

		function handler(e){
			e.preventDefault();
			self.changeState(0);
			self.showPost(this.getAttribute('data-id'));
			return false;
		}

	}

	/**
	 * Simple rest wrapper
	 * @param dataName
	 * @param onSuccess
	 * @param params
	 */
	function rest(dataName, onSuccess, params){
		window.helper.ajax('GET', dataName, params, function(response){
			onSuccess(response);
		})
	}

	return List;
})(window);