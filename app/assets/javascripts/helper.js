/**

 	A helper functions

 **/


window.helper = {};


/**
 * An Ajax wrapper, supports native XMLHttpRequest implementation only
 * @param url
 * @param method
 * @param params
 * @param onSuccess
 * @param onFailure
 */

window.helper.ajax = function(method, url, params, onSuccess, onFailure){
 	var req;
		params = null || params;

	try {
		req = new XMLHttpRequest();
	} catch (e){
		throw 'XMLHttpRequest unsupported'
	}

	req.open(method ,url, true);
	req.onreadystatechange  = stateHandler;
	req.send(params);

	function stateHandler(){
		if ((req.readyState == 4) && (req.status == 200)) {
			if(onSuccess && typeof(onSuccess) === 'function'){
				onSuccess(req.responseXML);
			}
		} else {
			if(onFailure && typeof(onFailure) === 'function'){
				onFailure(req.errorCode);
			}
		}
	}

};



/**
 * Extending one object with another
 * @param destination
 * @param source
 * @return {*}
 */
window.helper.extend = function(destination, source){
	for (var property in source)
		destination[property] = source[property];
	return destination;
};


/**
 * A xslt processor wrappper
 */
window.helper.xsltJobber = function(tpl, data){
	var result;

	if(window.ActiveXObject){
		var div = document.createElement('div');
		div.innerHTML = data.transformNode(tpl);
		result = div;

	} else if( window.XSLTProcessor){
		var processor = new XSLTProcessor();

		processor.importStylesheet(tpl);
		result = processor.transformToFragment(data, document).firstChild; // not really very safe :(
	}
	return result;
};


window.helper.formatTime = function(els){

	var i = 0,
		len = els.length,
		time;

	for(; i < len; i++){
		time = new Date(els[i].getAttribute('time'));
		els[i].innerHTML = time.toDateString();

	}
};



// addClass/removeClass heavily inspired by jQuery

window.helper.addClass = function(value){
	var classes, elem, cur, clazz, j,
		i = 0,
		len,
		el = this,
		proceed = typeof value === "string" && value;


	if(Object.prototype.toString.call(this) === '[object HTMLDivElement]'){
		el = [this];
	}

	len = el.length;

	if ( proceed ) {

		classes = ( value || "" ).match(/\S+/g) || [];

		for ( ; i < len; i++ ) {
			elem = el[ i ];
			cur = elem.nodeType === 1 && ( elem.className ?
				( " " + elem.className + " " ).replace( /[\t\r\n]/g, " " ) :
				" "
				);

			if ( cur ) {
				j = 0;
				while ( (clazz = classes[j++]) ) {
					if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
						cur += clazz + " ";
					}
				}

				elem.className = String.prototype.trim.call( cur );

			}
		}
	}

	return this;
};

window.helper.removeClass = function (value) {
	var classes, elem, cur, clazz, j,
		i = 0,
		el,
		len,
		proceed = arguments.length === 0 || typeof value === "string" && value;


	if (Object.prototype.toString.call(this) === '[object HTMLDivElement]') {
		el = [this];
	}

	len = el.length;

	if (proceed) {
		classes = ( value || "" ).match(/\S+/g) || [];

		for (; i < len; i++) {
			elem = el[ i ];

			cur = elem.nodeType === 1 && ( elem.className ?
				( " " + elem.className + " " ).replace(/[\t\r\n]/g, " ") :
				""
				);

			if (cur) {
				j = 0;
				while ((clazz = classes[j++])) {
					// Remove *all* instances
					while (cur.indexOf(" " + clazz + " ") >= 0) {
						cur = cur.replace(" " + clazz + " ", " ");
					}
				}
				elem.className = value ? String.prototype.trim.call(cur) : "";
			}
		}
	}

	return this;
};




