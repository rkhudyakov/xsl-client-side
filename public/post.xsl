<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" />
    <xsl:template match="post">
        <div class="post">
            <a href="#back_to_list" class="post__backLink" id="post__backLink">Back to all posts</a>
            <h1><xsl:value-of select="title"/></h1>
            <xsl:variable name="time" select="created-at" />
            <time time="{$time}" class="list__item__time"> </time>
            <p class="post__content text">
                <xsl:value-of select="content"/>
            </p>
        </div>
    </xsl:template>
</xsl:stylesheet>
