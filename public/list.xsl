<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" />
    <xsl:template match="/">
        <ul class="list">
            <xsl:for-each select="posts/post">
                <xsl:variable name="id" select="id" />
                <li class="list__item">
                    <a href="#expand_new" class="list__item__linkArea" data-id="{$id}">
                        <h2 class="list__item__title">
                            <xsl:value-of select="title"/>
                        </h2>
                        <p class="list__item__teaser text">
                            <xsl:value-of select="shortText"/>
                        </p>
                        <xsl:variable name="time" select="created-at" />
                        <time time="{$time}" class="list__item__time"> </time>
                    </a>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>
</xsl:stylesheet>
