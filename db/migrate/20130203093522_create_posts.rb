class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.string :shortText
      t.text :content
      t.string :importance

      t.timestamps
    end
  end
end
